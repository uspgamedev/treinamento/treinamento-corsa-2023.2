extends Area2D


@onready var ageing_component = $AgeingComponent


func save():
	return {
		"filename": self.scene_file_path,
		"parent": get_parent().get_path(),
		"pos_x": position.x,
		"pos_y": position.y,
		"age": ageing_component.current_age
	}
