extends Control

@onready var pause = get_tree().get_nodes_in_group("player")[5]
@onready var book = get_tree().get_nodes_in_group("player")[4]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(!pause.paused and !book.open):
		visible = true;
	else:
		visible = false;
	
	pass
