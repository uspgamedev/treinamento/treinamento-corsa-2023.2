extends RigidBody2D

var impulse_randomizer : RandomNumberGenerator = RandomNumberGenerator.new()


# Called when the node enters the scene tree for the first time.
func _ready():
	impulse_randomizer.randomize()
	var impulse: Vector2 = Vector2(impulse_randomizer.randi_range(-10,10), impulse_randomizer.randi_range(-150,-180))
	self.apply_impulse(self.position, impulse)

func _on_2DArea_body_entered(body: Node):
	if body.is_in_group("player"):
		


func _process(delta):
	pass
