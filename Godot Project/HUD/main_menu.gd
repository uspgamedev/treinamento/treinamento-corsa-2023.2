extends Node2D

@export var main_game_scene : PackedScene

func _on_new_game_button_button_up():
	get_tree().change_scene_to_file(main_game_scene.resource_path)
	visible = false


func _on_quit_button_button_down():
	get_tree().quit()
