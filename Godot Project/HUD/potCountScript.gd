extends Label

var potCount = 1;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if potCount < 10:
		self.text = "x0" + str(potCount);
	else:
		self.text = "x" + str(potCount);
	pass
