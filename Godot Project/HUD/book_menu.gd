extends CanvasLayer

@onready var pause = get_tree().get_nodes_in_group("player")[5]
@export var open : bool = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("Wiki"):
		open = !open
	
	if Input.is_action_just_pressed("Pause"):
		open = false
		visible = false
	
	if(open):
		visible = true;
	else:
		visible = false;
	pass
