extends Control

@export var next_scene : PackedScene

func _on_animation_player_animation_finished(_anim_name):
	get_tree().change_scene_to_file(next_scene.resource_path)

func _input(event):
	if event.is_action_pressed("Attack"):
		$AnimationPlayer.animation_finished.emit("text_animation")
