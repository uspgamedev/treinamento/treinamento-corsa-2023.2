extends CanvasLayer

@export var paused : bool = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(Input.is_action_just_pressed("Pause")):
		pauseGame()
	
	if(paused):
		get_tree().paused = true
		visible = true
	else:
		get_tree().paused = false
		visible = false

func pauseGame():
	paused = !paused


func _on_continue_button_up():
	pauseGame()


func _on_exit_button_up():
	get_tree().quit()
