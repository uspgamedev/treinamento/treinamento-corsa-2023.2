extends Control

@onready var sword = get_tree().get_nodes_in_group("player")[0].get_node("Sword")
@onready var bar1 = get_node("MagicCyan")
@onready var bar2 = get_node("MagicYellow")
@onready var bar3 = get_node("MagicPurple")
@onready var bar4 = get_node("MagicBeyond")
var currentMagic: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	currentMagic = sword.current_magic
	bar1.value = currentMagic
	bar2.value = currentMagic
	bar3.value = currentMagic
	bar4.value = currentMagic
	if currentMagic <= 100:
		bar1.visible = true
		bar2.visible = false
		bar3.visible = false
		bar4.visible = false	
	elif currentMagic > 100 and currentMagic <= 300:
		bar1.visible = false
		bar2.visible = true
		bar3.visible = false
		bar4.visible = false
	elif currentMagic > 300 and currentMagic <= 700:
		bar1.visible = false
		bar2.visible = false
		bar3.visible = true
		bar4.visible = false
	elif currentMagic > 700:
		bar1.visible = false
		bar2.visible = false
		bar3.visible = false
		bar4.visible = true
	
	pass
