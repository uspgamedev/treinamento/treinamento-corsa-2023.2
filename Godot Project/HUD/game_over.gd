extends CanvasLayer

signal ressurected

var dead: bool = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if PlayerVariables.is_dead:
		visible = true
		Engine.time_scale = 0
	else:
		visible = false
		Engine.time_scale = 1
		
	if Input.is_action_just_pressed("Ressurect") and PlayerVariables.is_dead == true:
		PlayerVariables.is_dead = false
		PlayerVariables.ressurect = true
	pass
