extends Label

@onready var sword = get_tree().get_nodes_in_group("player")[0].get_node("Sword")

var magic: int = 0
var barCount: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	magic = sword.current_magic
	if magic > 700:
		visible = true;
	else:
		visible = false;
		
	barCount = (magic - 300) / 400;
	
	if barCount < 10:
		self.text = "x0" + str(barCount);
	else:
		self.text = "x" + str(barCount);
	
	pass
