extends CanvasLayer

var open: int = 1;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("Pause"):
		open = open * -1;
	
	if open == -1:
		visible = true;
		Engine.time_scale = 0;
	else:
		visible = false;
		Engine.time_scale = 1;
	pass
