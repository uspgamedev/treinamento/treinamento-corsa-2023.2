extends Node2D

@onready var farm = $Farm
@onready var dungeon = $"Dungeon 1"
@onready var player = $PlayerCharacter
@onready var anchor = $"Dungeon 1 Anchor"


# Called when the node enters the scene tree for the first time.
func _ready():
	load_game(true)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if PlayerVariables.ressurect:
		var hud_control = get_node("HUD/Control")
		
		hud_control.process_mode = Node.PROCESS_MODE_DISABLED
		continue_game()
		#for child in hud_control.get_children():
		#	if child.get("health") != null:
		#		child.health = player.health_component
		#	if child.get("sword") != null:
		#		child.sword = player.sword]
		hud_control.sword = player.sword
		hud_control.get_node("BarCount").sword = player.sword
		hud_control.get_node("Life").health = player.health_component
		hud_control.process_mode = Node.PROCESS_MODE_INHERIT
		#var life_hud = get_node("/root/Hud/Control/Life")
		#life_hud.health = player.health_component
		#var cyan_hud = get_node("/root/Hud/Control/MagicCyan")
		#cyan_hud.sword = player.sword
		
		PlayerVariables.ressurect = false
		
	if PlayerVariables.on_farm:
		if Input.is_action_just_pressed("Plant"):
			var tilemap = farm.get_node("TileMap")
			var tile_pos = tilemap.local_to_map(tilemap.to_local(player.global_position))
			var tile_index = tilemap.get_cell_atlas_coords(0, tile_pos)
			if tile_index.x == 0 and tile_index.y == 0:
				var seed = load("res://plants/plant_base/plant_seed.tscn").instantiate()
				seed.global_position = tilemap.to_global(tilemap.map_to_local(tile_pos))
				farm.get_node("Plants").add_child(seed)

func _on_farm_gate_1_entered():
	PlayerVariables.on_farm = false
	var farm_data = farm.save_farm()
	# Get player's data and save
	var player_data = player.save()
	save_game(farm_data + [player_data])
	
	dungeon.reset_enemies()
	dungeon.process_mode = Node.PROCESS_MODE_INHERIT
	send_node_to(player, Vector2(0, 0))
	swap(dungeon, farm)


func _on_farm_gate_2_entered():
	pass # Replace with function body.


func _on_dungeon_1_exited():
	PlayerVariables.on_farm = true
	dungeon.process_mode = Node.PROCESS_MODE_DISABLED
	player.sword.current_magic = 0
	send_node_to(player, Vector2(0, 0))
	swap(dungeon, farm)


func _on_time_passed(amount):
	farm.pass_time(amount)


func send_node_to(node: Node2D, pos: Vector2):
	node.global_position = pos


func swap(node1: Node2D, node2: Node2D):
	var old_pos = node1.global_position
	send_node_to(node1, node2.global_position)
	send_node_to(node2, old_pos)


func save_game(data):
	var save_file = FileAccess.open("user://zeldalike.save", FileAccess.WRITE)
	for line in data:
		# Store the save dictionary as a new line in the save file.
		save_file.store_line(JSON.stringify(line))


func load_game(at_start: bool):
	if not FileAccess.file_exists("user://zeldalike.save"):
		return # Error! We don't have a save to load.dwdw

	# We need to revert the game state so we're not cloning objects
	# during loading. This will vary wildly depending on the needs of a
	# project, so take care with this step.
	# For our example, we will accomplish this by deleting saveable objects.
	farm.clear()
	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	var save_game = FileAccess.open("user://zeldalike.save", FileAccess.READ)
	while save_game.get_position() < save_game.get_length() - 123:
		var json_string = save_game.get_line()
		# Creates the helper class to interact with JSON
		farm.load(json_string)
		
	var json_string = save_game.get_line()
	# Creates the helper class to interact with JSON
	var json = JSON.new()
	# Check if there is any error while parsing the JSON string, skip in case of failure
	var parse_result = json.parse(json_string)
	# Get the data from the JSON object
	var node_data = json.get_data()
	
	if not at_start:
		player.queue_free()
		
		var new_object = load(node_data["filename"]).instantiate()
		get_node(node_data["parent"]).add_child(new_object)
		new_object.position = Vector2(node_data["pos_x"], node_data["pos_y"])
		new_object.health_component.current_health = node_data["health"]
		new_object.sword.current_magic = 0
		new_object.connect_ice(dungeon.icy_region_entered, dungeon.icy_region_exited)
	
		player = new_object
	else:
		player.position = Vector2(node_data["pos_x"], node_data["pos_y"])
		player.health_component.current_health = node_data["health"]
		player.sword.current_magic = node_data["magic"]
	

func continue_game():
	load_game(false)
	dungeon.global_position = anchor.global_position
	farm.global_position = Vector2(0, 0)
	PlayerVariables.on_farm = true
	var harvestable = load("res://plants/plant_base/plant_harvestable.tscn").instantiate()
	harvestable.global_position = farm.harvest_anchor.global_position
	harvestable.magic_restore_amount = 60
	farm.add_child(harvestable)


func connect_time(source):
	source.connect(_on_time_passed)
