extends Behavior

var start_position
var end_position

@export var speed = 128
@export var limit = 0.5

var move_direction : Vector2

func enter():
	start_position = character.position
	end_position = start_position + Vector2(0, 3*96)
	move_direction = end_position - character.position

	move.emit(move_direction.normalized(),speed)
	
	
func change_direction():
	var temp_end = end_position
	end_position = start_position
	start_position = temp_end

func update_velocity():
	move_direction = end_position - character.position
	
	if move_direction.length() < limit:
		change_direction()
		move_direction = end_position - character.position
		move.emit(move_direction.normalized(),speed)


func physics_update(_delta):
	update_velocity()
