class_name Behavior
extends Node

#Signal sent when transitioning
signal move
signal attack
signal player_detected
signal death

var playerdetected:bool = false
var player = null
var dead:bool = false
var hunting:bool = false

@onready var detection_range:DetectionRange = self.get_parent().get_node("DetectionRange")
@onready var hit_range:HitRange = self.get_parent().get_node("HitRange")
@onready var character:EnemyCharacter = self.get_parent()
@onready var wandering_timer = self.get_node("WanderingTimer")
@onready var attack_timer = self.get_node("AttackChargeTimer")
@onready var behavior_state_machine = self.get_node("BehaviorStateMachine")

#Function called when ready
func enter():
	pass

#Function called when there is a frame update
func update(_delta : float):
	pass

#Function called when there is a physics update
func physics_update(_delta : float):
	pass
