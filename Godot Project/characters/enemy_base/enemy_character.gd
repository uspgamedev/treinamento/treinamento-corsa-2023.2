extends CharacterBody2D
class_name EnemyCharacter

@export var magic_drain_amount: int = 3
@export var ageing_amount: int = 1

signal attack
signal dead
signal character_detected
signal pass_time

@onready var health_component: HealthComponent = $HealthComponent
@onready var behavior: Behavior = $Behavior

@onready var origin: Vector2 = position

@export var hurt_box:CollisionShape2D
@export var collision:CollisionShape2D
# Movement Speed
var speed : float = 0.0

# Variables that store the current direction and the last non Idle direction
var direction : Vector2 = Vector2(0.0,0.0)

var goal

func _ready():
	behavior.move.connect(on_move)
	behavior.attack.connect(on_attack)
	behavior.player_detected.connect(on_detected)
	behavior.enter()
	get_node("/root/Game").connect_time(pass_time)


func _physics_process(delta):
	# Sets the velocity vector
	velocity = speed * direction
	
	if behavior:
		behavior.physics_update(delta)
		goal = behavior.goal
	
	move_and_slide()


func on_move(new_direction : Vector2, new_speed : float):
	direction = new_direction.normalized()
	speed = new_speed


func drain_magic(sword_hitbox: SwordHitBox):
	var sword: Sword = sword_hitbox.get_sword()
	var enough = sword.drain_magic(magic_drain_amount)
	# Only takes damage if there is enough magic in the sword
	if not enough:
		return
	take_damage(1)
	health_component.take_damage(5)


func take_damage(_damage: int):
	pass
	#health_component.take_damage(damage)


func heal(amount: int):
	health_component.heal(amount)

func on_detected(detected):
	character_detected.emit(detected)
	
func on_health_depleted():
	dead.emit("death")
	pass_time.emit(ageing_amount)
	hurt_box.queue_free()
	collision.queue_free()
	speed = 0
	
func on_attack(_value):
	attack.emit(1)

func get_scene_path():
	return self.scene_file_path
