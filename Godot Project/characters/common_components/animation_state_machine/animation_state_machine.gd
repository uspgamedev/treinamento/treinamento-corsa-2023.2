extends StateMachines
class_name AnimationStateMachine

#Gets the animated sprite component
@export var animated_sprite : AnimatedSprite2D

#Gets the character body component
@export var character : CharacterBody2D
