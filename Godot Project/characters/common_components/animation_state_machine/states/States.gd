#Default States Class
extends Node
class_name States

#Signal sent when transitioning
signal transitioned 

#Function called when entered
func enter():
	pass

#Function called when exited
func exit():
	pass

#Function called when there is a frame update
func update(_delta : float):
	pass

#Function called when there is a physics update
func physics_update(_delta : float):
	pass
