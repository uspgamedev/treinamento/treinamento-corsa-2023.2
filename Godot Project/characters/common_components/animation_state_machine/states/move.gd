extends States
class_name Move

#Gets the character and it`s sprite
@onready var animated_sprite : AnimatedSprite2D = get_parent().animated_sprite
@onready var character : CharacterBody2D = get_parent().character

#Speed value
var speed : float 

func _ready():
	character.dead.connect(on_death)
	
func enter():
	animated_sprite.play("Move") #Selects animation
	
func update(_delta):
	pass
	
func physics_update(_delta):
	pass

func on_death(_message):
	transitioned.emit(self,"Death")
