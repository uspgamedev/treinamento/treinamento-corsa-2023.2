extends States
class_name HuntingRight

#Gets the character and it`s sprite
@onready var animated_sprite : AnimatedSprite2D = get_parent().animated_sprite
@onready var character : CharacterBody2D = get_parent().character

#Speed value
var speed : float 
var detected:bool=false

func _ready():
	character.dead.connect(on_death)
	character.attack.connect(on_attack)
	character.character_detected.connect(on_detection_change)
	
func enter():
	animated_sprite.play("HuntingRight") #Selects animation

func update(_delta):
	pass

func physics_update(_delta):
	#Character Stops
	if character.velocity == Vector2(0.0,0.0):
		transitioned.emit(self,"IdleEnemyRight")
	if not detected:
		transitioned.emit(self,"WanderingRight")
	#Character Stops the X movement
	elif abs(character.velocity.y)>abs(character.velocity.x):
		if character.velocity.y>0:
			transitioned.emit(self,"HuntingDown")
		else:
			transitioned.emit(self,"HuntingUp")
	elif character.velocity.x<=0:
		transitioned.emit(self,"HuntingLeft")

func on_detection_change(state):
	detected=state
func on_attack(_value):
	transitioned.emit(self,"AttackingRight")

func on_death(_message):
	transitioned.emit(self,"DeathRight")
