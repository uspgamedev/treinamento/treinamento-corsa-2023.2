extends States
class_name DeathLeft

#Gets the character and it`s sprite
@onready var animated_sprite : AnimatedSprite2D = get_parent().animated_sprite
@onready var character : CharacterBody2D = get_parent().character



func enter():
	animated_sprite.play("DeathLeft") #Selects animation
	
func update(_delta):
	pass
	
func physics_update(_delta):
	pass
