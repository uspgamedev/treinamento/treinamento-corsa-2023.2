extends States
class_name AttackingLeft

#Gets the character and it`s sprite
@onready var animated_sprite : AnimatedSprite2D = get_parent().animated_sprite
@onready var character : CharacterBody2D = get_parent().character

# Called when the node enters the scene tree for the first time.
func _ready():
	character.dead.connect(on_death)
	animated_sprite.animation_finished.connect(on_animation_finished)
func enter():
	animated_sprite.play("AttackingLeft") #Selects animation
	
func update(_delta):
	var facing = character.position-character.goal
	if abs(facing.x)>abs(facing.y):
		if facing.x<0:
			transitioned.emit(self,"AttackingRight")
		else:
			transitioned.emit(self,"AttackingLeft")
	elif facing.y>0:
		transitioned.emit(self,"AttackingUp")
	else:
		transitioned.emit(self,"AttackingDown")
	pass
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
func on_animation_finished():
	transitioned.emit(self,"HuntingLeft")

func on_death(_message):
	transitioned.emit(self,"DeathLeft")
