extends States
class_name MoveDown

#Gets the character and it`s sprite
@onready var animated_sprite : AnimatedSprite2D = get_parent().animated_sprite
@onready var character : CharacterBody2D = get_parent().character

#Speed value
var speed : float 

func _ready():
	character.dead.connect(on_death)
	
func enter():
	animated_sprite.play("MoveDown") #Selects animation
	
func update(_delta):
	pass


func physics_update(_delta):
	#Character Stops
	if character.velocity == Vector2(0.0,0.0):
		transitioned.emit(self,"IdleDown")
		
	#Character Stops the Y movement
	elif abs(character.velocity.y)<abs(character.velocity.x):
		if character.velocity.x>0:
			transitioned.emit(self,"MoveRight")
		else:
			transitioned.emit(self,"MoveLeft")
	elif character.velocity.y<=0:
		transitioned.emit(self,"MoveUp")

func on_death(_message):
	transitioned.emit(self,"DeathDown")
