extends States
class_name MoveLeft

#Gets the character and it`s sprite
@onready var animated_sprite : AnimatedSprite2D = get_parent().animated_sprite
@onready var character : CharacterBody2D = get_parent().character

#Speed value
var speed : float 

func _ready():
	character.dead.connect(on_death)

func enter():
	animated_sprite.play("MoveLeft") #Selects animation
	
func update(_delta):
	pass

	
func physics_update(_delta):
	#Character Stops
	if character.velocity == Vector2(0.0,0.0):
		transitioned.emit(self,"IdleLeft")
		
	#Character Stops the X movement
	elif abs(character.velocity.y)>abs(character.velocity.x):
		if character.velocity.y>0:
			transitioned.emit(self,"MoveDown")
		else:
			transitioned.emit(self,"MoveUp")
	elif character.velocity.x>=0:
		transitioned.emit(self,"MoveRight")

func on_death(_message):
	transitioned.emit(self,"DeathLeft")
