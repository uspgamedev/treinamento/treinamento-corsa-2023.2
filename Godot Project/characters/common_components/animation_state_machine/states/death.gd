extends States
class_name Death

#Gets the character and it`s sprite
@onready var animated_sprite : AnimatedSprite2D = get_parent().animated_sprite
@onready var character : CharacterBody2D = get_parent().character



func enter():
	animated_sprite.play("Death") #Selects animation
	
func update(_delta):
	pass
	
func physics_update(_delta):
	pass
