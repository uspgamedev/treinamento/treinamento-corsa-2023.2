extends States
class_name IdleLeft

#Gets the character and it`s sprite
@onready var animated_sprite : AnimatedSprite2D = get_parent().animated_sprite
@onready var character : CharacterBody2D = get_parent().character

func _ready():
	character.dead.connect(on_death)

func enter():
	animated_sprite.play("IdleLeft") #Selects animation
	
func physics_update(_delta):
	#If movement starts on the X axis
	if abs(character.velocity.x)>abs(character.velocity.y):
		if character.velocity.x > 0:
			transitioned.emit(self,"MoveRight")
		else:
			transitioned.emit(self,"MoveLeft")
	
	#If movement starts on the Y axis
	elif abs(character.velocity.x)<abs(character.velocity.y):
		if character.velocity.y > 0:
			transitioned.emit(self,"MoveDown")
		else:
			transitioned.emit(self,"MoveUp")
	
	#If movement starts on the Diagonal
	elif character.velocity.length():
		if character.velocity.x > 0:
			transitioned.emit(self,"MoveRight")
		else:
			transitioned.emit(self,"MoveLeft")

func on_death(_message):
	transitioned.emit(self,"DeathLeft")
