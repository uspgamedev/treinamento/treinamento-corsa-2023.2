extends States
class_name IdleEnemyDown

#Gets the character and it`s sprite
@onready var animated_sprite : AnimatedSprite2D = get_parent().animated_sprite
@onready var character : CharacterBody2D = get_parent().character

func _ready():
	character.dead.connect(on_death)
	character.attack.connect(on_attack)


func enter():
	animated_sprite.play("IdleDown") #Selects animation


func physics_update(_delta):
	#If movement starts on the X axis
	if abs(character.velocity.x)>abs(character.velocity.y):
		if character.velocity.x > 0:
			transitioned.emit(self,"WanderingRight")
		else:
			transitioned.emit(self,"WanderingLeft")
	
	#If movement starts on the Y axis
	elif abs(character.velocity.x)<abs(character.velocity.y):
		if character.velocity.y > 0:
			transitioned.emit(self,"WanderingDown")
		else:
			transitioned.emit(self,"WanderingUp")
	
	#If movement starts on the Diagonal
	elif character.velocity.length():
		if character.velocity.x > 0:
			transitioned.emit(self,"WanderingRight")
		else:
			transitioned.emit(self,"WanderingLeft")

func on_attack(_value):
	transitioned.emit(self,"AttackingDown")

func on_death(_message):
	transitioned.emit(self,"DeathDown")
