class_name HurtBox
extends Area2D


func _ready():
	self.connect("area_entered", _on_hurtbox_entered)


func _on_hurtbox_entered(area: Area2D):
	if area is HitBox:
		if area is SwordHitBox:
			# An entity must have ONLY ONE of the following methods
			if area.get_parent().get_parent().get_parent().sword_on:
				if owner.has_method("drain_magic"):
					owner.drain_magic(area)
				elif owner.has_method("restore_magic"):
					owner.restore_magic(area)
		else:
			if owner.has_method("take_damage"):
				owner.take_damage(area.damage)
	#if area is Item:
	#	pass
