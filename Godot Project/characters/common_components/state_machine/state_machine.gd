extends Node
class_name StateMachines

#Gets the initial state component
@export var initial_state:States
signal changed_state

#Current State
var current_state : States

#Dictionary of States
var states : Dictionary = {}

func _ready():
	#Get all states that are this node's children
	for child in get_children():
		if child is States:
			states[child.name.to_lower()]=child
			child.transitioned.connect(on_child_transition)
	
	#Enters the initial state
	if initial_state:
		initial_state.enter()
		current_state = initial_state

func _process(delta):
	#Updates current state
	if current_state:
		current_state.update(delta)

func _physics_process(delta):
	#Updates current state
	if current_state:
		current_state.physics_update(delta)

#Transitions between states
func on_child_transition(state : States ,new_state_name : String):
	
	#Checks if the state requesting transition is the current state
	if state != current_state:
		return
	
	#Sets the new state
	var new_state = states.get(new_state_name.to_lower())
	
	#Checks if the new state exists
	if !new_state:
		return
	
	#Calls the exit function of the current state
	if current_state:
		current_state.exit()
	
	#Calls the enter function of the new state
	new_state.enter()
	
	#Current state becomes the new state
	current_state=new_state
	changed_state.emit(new_state_name.to_lower())
