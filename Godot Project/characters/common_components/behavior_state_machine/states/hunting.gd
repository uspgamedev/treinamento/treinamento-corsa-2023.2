extends States
class_name Hunting

@onready var current_character:EnemyCharacter = self.get_owner()
@onready var detection_range:DetectionRange = current_character.get_node("DetectionRange")
@onready var hit_range:HitRange = current_character.get_node("HitRange")
@onready var behavior = current_character.get_node("Behavior")

func _ready():
	detection_range.character_detected.connect(on_detection_change)
	hit_range.character_in_range.connect(on_character_hit_range_change)
	#current_character.dead.connect(on_death)
	behavior.death.connect(on_death)
	behavior.in_attack_range.connect(in_attack_range)
	pass

func enter():
	pass
	
func on_detection_change(detected:bool,_body):
	if not detected:
		transitioned.emit(self,"Wandering")

func on_character_hit_range_change(detected:bool):
	if detected:
		transitioned.emit(self,"Attacking")

func on_death(_dead):
	transitioned.emit(self,"Dead")
	

func in_attack_range():
	transitioned.emit(self,"Attacking")
