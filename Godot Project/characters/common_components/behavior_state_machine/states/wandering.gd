extends States
class_name Wandering

@onready var current_character:EnemyCharacter = self.get_owner()
@onready var detection_range:DetectionRange = current_character.get_node("DetectionRange")
@onready var hit_range:HitRange = current_character.get_node("HitRange")
@onready var behavior = current_character.get_node("Behavior")

func _ready():
	detection_range.character_detected.connect(on_detection_change)
	hit_range.character_in_range.connect(on_character_hit_range_change)
	#current_character.dead.connect(on_death)
	behavior.death.connect(on_death)

	pass

func enter():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func on_detection_change(detected:bool,_body):
	if detected:
		transitioned.emit(self,"Hunting")

func on_character_hit_range_change(detected:bool):
	if detected:
		transitioned.emit(self,"Attacking")

func on_death(_dead):
	transitioned.emit(self,"Dead")


