class_name HealthComponent
extends Node

#Health Component Variables
@export var max_health: int = 20
@export var current_health: int = 20

#Signals sent by health events
signal health_depleted
signal health_maxed
signal health_changed(change: int)


#Function is called when the signal heal(amount) is sent
func heal(amount : int)->void:
	#Calculates new current health
	var new_health: float = current_health + amount
	current_health = clamp(new_health, 0.0, max_health)
	
	#Since health has changed, sends health_changed signal
	health_changed.emit(amount)
	
	#If health is maxed sends health_maxed signal
	if current_health == max_health:
		health_maxed.emit()

#Function is called when the signal heal(amount) is sent
func take_damage(amount: int):
	#Calculates new current health
	var new_health: float = current_health - amount
	current_health = clamp(new_health, 0, max_health)
	
	#Since health has changed, sends health_changed signal
	health_changed.emit(amount)
	
	#If health is 0 sends health_depleted signal
	if current_health == 0:
		emit_signal("health_depleted")
