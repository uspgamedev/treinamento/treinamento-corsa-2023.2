extends CharacterBody2D

@onready var goal:Vector2 = get_parent().goal
@export var speed : float = 1
@onready var sprite:=$Sprite2D
@onready var timer = $Lifetime

# Variables that store the current direction and the last non Idle direction
var direction : Vector2 = Vector2(0.0,0.0)
# Called when the node enters the scene tree for the first time.
func _ready():
	direction = goal - get_parent().position 
	#direction = direction.normalized()
	self.set_rotation(PI+direction.angle())
	timer.timeout.connect(on_timeout)
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	# Sets the velocity vector
	velocity = speed * direction
	
	move_and_slide()

func on_timeout():
	self.queue_free()
