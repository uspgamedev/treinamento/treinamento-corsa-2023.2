extends Behavior
class_name ShooterBehavior


@export var speed = 200
signal in_attack_range
signal attack_end
var player_in_attack_range:bool = false
var goal = null
func _ready():

	detection_range.character_detected.connect(on_detection_change)
	hit_range.character_in_range.connect(on_character_hit_range_change)
	wandering_timer.timeout.connect(on_wander_timeout)
	attack_timer.timeout.connect(on_attack_timeout)
	character.dead.connect(on_death)
	behavior_state_machine.changed_state.connect(on_state_change)



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if playerdetected:
			goal = player.position
	if hunting:
		if playerdetected:
			move.emit(goal - self.get_parent().position,speed)
	pass

func on_detection_change(detected:bool,body):
	playerdetected = detected
	player = body
	player_detected.emit(detected)
	
	
func on_character_hit_range_change(_detected:bool):
	
	player_in_attack_range = not player_in_attack_range
	pass
	
	
func on_wander_timeout():
	var stop = randi_range(0,1)
	var new_direction = Vector2(randf_range(-1,1),randf_range(-1,1))
	if stop:
		move.emit(Vector2(0.0,0.0),0.0)
	else:
		move.emit(new_direction,speed/5.0)

func on_attack_timeout():
	#attack.emit(1)
	attack_end.emit()
	pass
			
func on_death(_message):
	move.emit(Vector2(0.0,0.0),0.0)
	dead=true
	death.emit(dead)

func on_state_change(state):
	wandering_timer.stop()
	attack_timer.stop()
	hunting = false
	match state:
		"attacking":
			move.emit(Vector2(0.0,0.0),0.0)
			attack_timer.start()
			attack.emit(1)
			pass
		"wandering":
			move.emit(Vector2(0.0,0.0),0.0)
			wandering_timer.start()
			pass
		"hunting":
			hunting=true
			if player_in_attack_range:
				in_attack_range.emit()
			pass
		"dead":
			pass
		_:
			pass
