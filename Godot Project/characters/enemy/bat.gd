extends CharacterBody2D

var start_position
var end_position

@export var speed = 128
@export var limit = 0.5

@onready var animations = $AnimationPlayer


func _ready():
	start_position = position
	end_position = start_position + Vector2(0, 3*96)


func changeDirection():
	var temp_end = end_position
	end_position = start_position
	start_position = temp_end


func update_velocity():
	var move_direction = end_position - position
	if move_direction.length() < limit:
		changeDirection()
		
	velocity = move_direction.normalized()*speed


func update_animation():
	if velocity.length() == 0:
		if animations.is_playing():
			animations.stop()
	else: 
		var direction = "Down"
		if velocity.x < 0: direction = "Left"
		elif velocity.x > 0: direction = "Right"
		elif velocity.y < 0: direction = "Up"
		
		animations.play("walk" + direction)


func _physics_process(_delta):
	update_velocity()
	move_and_slide()
	update_animation()
