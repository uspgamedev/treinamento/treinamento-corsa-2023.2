extends Area2D
class_name DetectionRange

signal character_detected
# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect("body_entered", _on_hurtbox_entered)
	self.connect("body_exited", _on_hurtbox_exited)


func _on_hurtbox_entered(body):
	if body is Player:
		character_detected.emit(true,body)

func _on_hurtbox_exited(body):
	if body is Player:
		character_detected.emit(false,null)
