extends Area2D
class_name HitRange

signal character_in_range
# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect("body_entered", _on_hurtbox_entered)
	self.connect("body_exited", _on_hurtbox_exited)


func _on_hurtbox_entered(body):
	if body is Player:
		character_in_range.emit(true)

func _on_hurtbox_exited(body):
	if body is Player:
		character_in_range.emit(false)
