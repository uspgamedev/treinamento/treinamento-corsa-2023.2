extends Attack
class_name PumpkinAttack

var hitbox:CollisionShape2D
# Called when the node enters the scene tree for the first time.
@onready var timer:Timer = self.get_node("AttackDurationTimer")

func _ready():
	character.attack.connect(_on_attack)
	hitbox = character.get_node("HitBox/CollisionShape2D")
	timer.timeout.connect(timer_end)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _on_attack(_value):
	timer.start()
	hitbox.disabled=false

func timer_end():
	hitbox.disabled=true
