class_name Sword
extends Node2D

@export var current_magic: int = 800
@export var max_magic: int = 10000

@onready var _pivot: Marker2D = $Pivot
@onready var _attack_animation: AnimationPlayer = $AttackAnimationPlayer
@onready var _vfx_animation: AnimationPlayer = $VFXAnimationPlayer
@onready var _player: Player = owner
@onready var _radius: float = $MovementRadius.shape.radius


func use() -> void:
	_attack_animation.play("attack")


func _physics_process(delta: float) -> void:
	var mouse_position := get_global_mouse_position()

	_pivot.look_at(mouse_position)
	_pivot.position.y = sin(Time.get_ticks_msec() * delta * 0.20) * 10

	# Flip pivot to avoid upside down attacks
	if mouse_position.x - global_position.x < 0:
		_pivot.scale.y = -1
	else:
		_pivot.scale.y = 1
	
	var player_position = _player.global_transform.origin 
	var distance = player_position.distance_to(mouse_position) 
	var mouse_dirrection = (mouse_position - player_position).normalized()
	if distance > _radius:
		mouse_position = player_position + (mouse_dirrection * _radius)
	_pivot.global_transform.origin = mouse_position


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("Attack"):
		use()


func drain_magic(amount: int):
	var enough := current_magic >= amount
	if enough:
		current_magic -= amount
		_vfx_animation.play("drain")
	return enough
	
	
func restore_magic(amount: int):
	current_magic += amount
	if current_magic > max_magic:
		current_magic = max_magic
