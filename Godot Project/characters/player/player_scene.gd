class_name Player
extends CharacterBody2D

#Health related signals (move to collision component?)
signal dead
signal attack

@onready var health_component: HealthComponent = $HealthComponent
@onready var sword: Sword = $Sword

#Movement Speed
const SPEED = 300.0
var sword_on:bool = true
#Variables that store the current direction and the last non Idle direction
var direction := Vector2(0.0,0.0)

var death = 1

var on_ice = false

#Function moves in any direction on the plane
func eight_directional_movement(value_x:float,value_y:float):
	direction = Vector2(value_x, value_y)
	
	#Normalizes the direction.
	if direction.length() > 1.0:
		direction = direction.normalized()


func _physics_process(_delta):
	#Gets the X component of the direction from the input.
	var input_x := Input.get_action_strength("Right") - Input.get_action_strength("Left")
	
	#Gets the Y component of the direction from the input.
	var input_y := Input.get_action_strength("Down") - Input.get_action_strength("Up")
	
	if (not on_ice) or (velocity.x == 0 and velocity.y == 0):
		eight_directional_movement(input_x,input_y)
	
	#Sets the velocity vector
	velocity = SPEED * direction * death
	
	move_and_slide()


func take_damage(damage: int):
	health_component.take_damage(damage)
	$AnimationPlayer.play("damage")


func heal(amount: int):
	health_component.heal(amount)


func on_health_depleted():
	death = 0
	dead.emit("Death")
	PlayerVariables.is_dead = true


func save():
	return {
		"filename": self.scene_file_path,
		"parent": get_parent().get_path(),
		"pos_x": 0,
		"pos_y": 0,
		"health": health_component.current_health,
		"magic": sword.current_magic
	}


func _on_dungeon_1_icy_region_entered():
	on_ice = true


func _on_dungeon_1_icy_region_exited():
	on_ice = false
	
	
func connect_ice(entered, exited):
	entered.connect(_on_dungeon_1_icy_region_entered)
	exited.connect(_on_dungeon_1_icy_region_exited)

	
func _drain_magic(amount:int):
	sword.drain_magic(amount)

func _get_magic():
	return sword.current_magic
	
func _toggle_sword():
	sword_on = not sword_on
	if sword_on:
		sword.visible = true
	else:
		sword.visible = false
	
