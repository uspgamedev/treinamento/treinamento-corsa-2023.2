extends Node2D

signal gate_1_entered
signal gate_2_entered

@onready var plants = $Plants
@onready var harvest_anchor = $HarvestAnchor

func _on_gate_1_area_entered(area):
	if area.owner is Player:
		gate_1_entered.emit()


func _on_gate_2_area_entered(area):
	if area.owner is Player:
		gate_2_entered.emit()


func save_farm():
	var result = []
	for node in plants.get_children():
		if node.scene_file_path.is_empty():
			print("persistent node '%s' is not an instanced scene, skipped" % node.name)
			continue
			
		if not node.has_method("save"):
			print("persistent node '%s' is missing a save() function, skipped" % node.name)
			continue
			
		var node_data = node.call("save")
		result.append(node_data)
	return result
	
	
func load(json_string):
	var json = JSON.new()
	# Check if there is any error while parsing the JSON string, skip in case of failure
	var parse_result = json.parse(json_string)
	if parse_result == OK:
		# Get the data from the JSON object
		var node_data = json.get_data()
		# Firstly, we need to create the object and add it to the tree and set its position.
		var new_object = load(node_data["filename"]).instantiate()
		get_node(node_data["parent"]).add_child(new_object)
		new_object.position = Vector2(node_data["pos_x"], node_data["pos_y"])
		if new_object.get("ageing_component") != null: 
			new_object.ageing_component.current_age = node_data["age"]
			
			
func clear():
	for plant in plants.get_children():
		plant.queue_free()


func pass_time(amount):
	for node in plants.get_children():
		if node.get("ageing_component") != null: 
			node.ageing_component.current_age += amount
