extends Node2D

signal icy_region_entered
signal icy_region_exited
signal exited

@onready var enemies = $Enemies

func _ready():
	process_mode = Node.PROCESS_MODE_DISABLED


func _on_ice_area_entered(area):
	if area.owner is Player:
		icy_region_entered.emit()


func _on_ice_area_exited(area):
	if area.owner is Player:
		icy_region_exited.emit()

func _on_totem_interacted():
	# Sends the player home, for now
	# Should reset entities
	exited.emit()

func reset_enemies():
	# Get enemy origin and create new instance on the origin
	for enemy in enemies.get_children():
		var origin = enemy.origin
		var path = enemy.get_scene_path()
		enemy.queue_free()
		var new_enemy = load(path).instantiate()
		new_enemy.global_position = origin
		enemies.add_child(new_enemy)
