extends Area2D

var inside := false

signal is_ready


func _process(delta):
	if inside:
		if Input.is_action_just_pressed("Interact"):
			is_ready.emit()


func _on_area_entered(area):
	if area.owner is Player:
		# is_ready.emit()
		inside = true


func _on_area_exited(area):
	if area.owner is Player:
		# is_ready.emit()
		inside = false
