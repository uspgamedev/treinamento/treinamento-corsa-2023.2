extends "res://interactables/base/action.gd"

@export var animated_sprite : AnimatedSprite2D
@export var charging_step_timer : Timer
@export var cost : int = 1
@onready var player : Player = get_parent().character

var toggled:bool = false
func _ready():
	charging_step_timer.timeout.connect(timer_end)
	
func interact():
	player = get_parent().character
	var _value = player._get_magic()
	toggled = not toggled
	player._toggle_sword()
	if toggled:
		animated_sprite.play("Charging")
		charging_step_timer.start()
	else:
		animated_sprite.play("Not Charging")
		charging_step_timer.stop()
	pass

func timer_end():
	if player:
		var _value = player._get_magic()
		if _value >= cost:
			player._drain_magic(cost)
