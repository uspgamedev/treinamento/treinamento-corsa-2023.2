extends "res://interactables/base/action.gd"

@export var animated_sprite : AnimatedSprite2D
@export var collision_area : CollisionShape2D

@onready var player : Player = get_parent().character

@export var cost : int = 10

func interact():
	player = get_parent().character
	if player:
		var _value = player._get_magic()
		if _value>=cost and player.sword_on:
			player._drain_magic(cost)
			activated.emit()
			collision_area.disabled=true
			animated_sprite.play("Open")
			get_parent()._clickable_area.input_pickable = false
