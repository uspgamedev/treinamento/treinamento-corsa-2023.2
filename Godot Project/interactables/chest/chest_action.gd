extends "res://interactables/base/action.gd"

@export var animated_sprite : AnimatedSprite2D
# Called when the node enters the scene tree for the first time.
func interact():
	activated.emit()
	animated_sprite.play("Open")
