extends Node


func save_game():
	var save_file = FileAccess.open("user://zeldalike_2023_2.save", FileAccess.WRITE)
	var nodes = get_tree().get_nodes_in_group("Persist")
	for node in nodes:
		if node.scene_file_path.is_empty():
			print("persistent node '%s' is not an instanced scene, skipped" % node.name)
			continue
			
		if not node.has_method("save"):
			print("persistent node '%s' is missing a save() function, skipped" % node.name)
			continue
			
		var node_data = node.call("save")
		var json_string = JSON.stringify(node_data)
		save_file.store_line(json_string)
		
	save_file.close()


func load_game():
	if not FileAccess.file_exists("user://zeldalike_2023_2.save"):
		print("There are no save files to load")
		return
	
	# The loading process creates new instances, so existing nodes in the
	# "Persist" group must be removed to avoid duplicates
	var nodes = get_tree().get_nodes_in_group("Persist")
	for node in nodes:
		node.queue_free()
		
	var save_file = FileAccess.open("user://zeldalike_2023_2.save", FileAccess.READ)
	while save_file.get_position() < save_file.get_length():
		var json_string = save_file.get_line()
		
		var json = JSON.new()
		var parse_result = json.parse(json_string)
		if not parse_result == OK:
			print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
			continue
			
		var data = json.get_data()
		
		var instance = load(data["filename"]).instantiate()
		get_node(data["parent"]).add_child(instance)
		instance.position = Vector2(data["pos_x"], data["pos_y"])
		
		for key in data.keys():
			if key == "name" or key == "parent" or key == "pos_x" or key == "pos_y":
				continue
				
			instance.set(key, data[key])
		
